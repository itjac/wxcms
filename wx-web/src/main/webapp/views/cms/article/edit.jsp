<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <% Date d = new Date();%>
    <% SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//24小时制  %>
    <%String date = sdformat.format(d); %>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}/static/css/ztree/metro/ztree.css"  media="all">

    <%--百度编辑器需要引入的js--%>
    <script type="text/javascript" charset="utf-8" src="${basePath}/static/plugins/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${basePath}/static/plugins/ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="${basePath}/static/plugins/ueditor/lang/zh-cn/zh-cn.js"></script>
    <style>
        .admin-main{
            height: 900px;
        }
        .mainLeft{
            float: left;
        }
        .mainRight{
            float: left;
        }
    </style>

</head>
<body>
<div class="admin-main">
    <div class="mainLeft">
    <form id="articleForm" method="post" class="layui-form" action="">
           <input value="${article.article_pk}" id="article_pk" name="article_pk" type="hidden" />

           <div class="layui-form-item" style="margin-top:2%;">
               <label class="layui-form-label">标题</label>
               <div class="layui-input-inline">
                   <input value="${article.article_title}" id="article_title" name="article_title"  lay-verify="required"  autocomplete="off" placeholder="请输入标题" class="layui-input" type="text" />
               </div>

               <label class="layui-form-label">图片</label>
               <div class="layui-input-inline">
                   <img id="images" src="${article.article_titleimg}" width="190px" height="45px" title="">
               </div>

               <label class="layui-form-label">图片</label>
               <div class="layui-input-inline">
                   <input value="${article.article_titleimg}" id="article_titleimg" name="article_titleimg"  autocomplete="off"  class="layui-input" type="hidden"/>
                   <button type="button" class="layui-btn" id="file">
                       <i class="layui-icon">&#xe67c;</i>上传图片
                   </button>
               </div>

           </div>

            <div class="layui-form-item">

                <label class="layui-form-label">链接</label>
                <div class="layui-input-inline">
                    <input value="${article.article_url}" id="article_url" name="article_url"  lay-verify="required"  autocomplete="off" placeholder="请输入链接" class="layui-input" type="text" />
                </div>

                <label class="layui-form-label">发文日期</label>
                <div class="layui-input-inline">
                    <input value="${article.article_sendtime}" id="article_sendtime" name="article_sendtime" class="layui-input">
                </div>

            </div>
            <div align="center">
                <textarea id="editor" name="editor" style="margin-top:2%;padding:0;width:890px;height:650px;">${article.article_content}</textarea>
            </div>

    </form>
</div>
    <%--树形菜单--%>
<div class="mainRight">
    <div align="center" class="layui-input-block" style="margin: 5% auto">
        <button class="layui-btn layui-btn-small" align="center" id="edit">保存</button>
        <%--<button class="layui-btn layui-btn-small" align="center" id="send">发布</button>--%>
    </div>
    <fieldset class="layui-elem-field layui-field-title">
        <legend>菜单</legend>
    </fieldset>
    <ul id="ztree" class="ztree" style="display: inline-block; width: 170px; height: 750px; padding: 10px; border: 1px solid #ddd; overflow: auto;">
    </ul>
</div>
</div>
</body>

<script>
    /**
     * 初始化百度编辑器
     *
     * */
    var ue = UE.getEditor('editor', {
        autoHeight:true,
        autoHeightEnabled: false,//是否自动长高
        autoFloatEnabled: false,//浮动是离浏览器的高度
        imageScaledEnable:true,//图片能否自动缩放
        allowDivTransToP:false//是否容许div转换为p 标签
//        enterTag : 'br'
    });
    /**
     * 设置百度编辑器上传action
     *
     * */
    UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl = function(action) {
        console.log("action"+action)
        if (action == 'uploadimage' || action == 'uploadscrawl' ) {
            return '<%=path%>/ueditor/upimage';
        }else if (action == 'uploadvideo')
        {
            return '<%=path%>/ueditor/upvideo';
        }
        else if (action == 'uploadfile')
        {
            return '<%=path%>/ueditor/upfile';
        }
        else {
            return this._bkGetActionUrl.call(this, action);
        }
    };
</script>

<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<%--layui--%>
<script>
    layui.config({
        base: '${basePath}/static/js/'
    }).use(['form','laydate','upload','ztree'],function () {
        var $ = layui.jquery;
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,upload = layui.upload
            ,laydate = layui.laydate;

        //时间选择器
        laydate.render({
            elem: '#article_sendtime'
            ,type: 'datetime'
            ,format: 'yyyy-MM-dd HH:mm:ss' //可任意组合
        });


        <%--$(document).ready(function (){--%>
            <%--var article_pk=$("#article_pk").val();--%>
            <%--if(article_pk!=null && article_pk!=''){--%>
               <%--$.ajax({--%>
                   <%--type:'get',--%>
                   <%--url:"<%=path%>/article/list",--%>
                   <%--data:{article_pk:article_pk},--%>
                   <%--async : true,--%>
                   <%--success:function (response) {--%>
                       <%--var json = eval(response); // 数组--%>
                       <%--$.each(json,function (i) {--%>
                           <%--$("#article_pk").val(json[i].article_pk);--%>
                           <%--$("#article_title").val(json[i].article_title);--%>
                           <%--$("#article_sendtime").val(json[i].article_sendtime);--%>
                           <%--$("#images").attr("src",json[i].article_titleimg);--%>
                           <%--$("#images").attr("title",json[i].article_titleimg);--%>
                           <%--$("#article_titleimg").val(json[i].article_titleimg);--%>
                           <%--ue.setContent(json[i].article_content);--%>
                       <%--});--%>
                       <%--form.render('select');--%>
                   <%--}--%>
               <%--});--%>

            <%--}--%>

        <%--});--%>



        //文件上传
        var uploadInst = upload.render({
            elem: '#file' //绑定元素
            ,url: '<%=path%>/file/upload' //上传接口
            ,done: function(res){
                //上传完毕回调
                if (res.state=="success")
                {
                    $("#images").attr("src",res.msg);
                    $("#article_titleimg").val(res.msg)
                }
                else {
                    parent.layer.alert("上传失败");
                    return false;
                }
            }
            ,error: function(){
                //请求异常回调
            }
        });


        var setting = {
            view: {
//                addHoverDom: addHoverDom,
//                removeHoverDom: removeHoverDom,
                selectedMulti: false
            },
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            edit: {
                enable: false
            },
            callback: {
                onCheck:onCheck
            }
        };

        var jsonArray= [];
        var ids ="";
        function onCheck(e,treeId,treeNode){
            var treeObj=$.fn.zTree.getZTreeObj("ztree"),
                nodes=treeObj.getNodes(true);
            jsonArray=[];
            for(var i=0;i<nodes.length;i++){
                jsonArray.push(nodes[i]);
            }
            console.log(JSON.stringify(jsonArray));
        }

        /**
         * 获取自动赋值选中节点的值
         * */
        function getCheck() {
            var treeObj=$.fn.zTree.getZTreeObj("ztree");
            console.log(treeObj);
            var nodes=treeObj.getCheckedNodes(true);
            ids = "";
            for(var i=0;i<nodes.length;i++){
                //debugger;
                if (nodes[i].isParent!=true){
                    ids+=nodes[i].id+",";
                }
            }
        }

        $(document).ready(function() {
            var list_url = "${basePath}/channel/article_ztree_list";
            var roles_pk=$("#roles_pk").val();
            $.ajax({
                type:'post',
                url:list_url,
                data:{roles_pk:roles_pk},
                success:function (response) {
                    $.fn.zTree.init($("#ztree"), setting, response);
                    getztree_checked();
                }
            })

        });


        /**
         *
         * 获取文章对应的树形菜单
         * */
        function getztree_checked() {
            var article_pk = $("#article_pk").val();
            if (article_pk != null && article_pk != "") {
                var url = "<%=path%>/article/ztree_checkd";
                $.ajax({
                    type: 'post',
                    url: url,
                    data: {
                        article_pk: article_pk
                    },
                    success: function (response) {
                        console.log("response"+response);
                        set_checked(response);//设置树形菜单选中
                    },
                    error: function (response) {
                        parent.layer.alert("获取选中菜单错误！");
                    }
                })
            }
            return false;
        }

        /**
         *
         * 设置选中
         * */
        function set_checked(response) {
           var treeObj=$.fn.zTree.getZTreeObj("ztree");
            //遍历每一个节点然后动态更新nocheck属性值
            var json=eval(response); //数组
            $.each(json,function (j) {
                var node = treeObj.getNodeByParam("id", json[j].fk_channel_pk, null);
                if(node!=null){
                    treeObj.checkNode(node, true, true);//checkbox打钩选中效果
                    treeObj.selectNode(node, true, true);//鼠标选中效果
                    treeObj.expandNode(node, true, false);//指定选中ID节点展开
                }
            })
        }


        $("#edit").on('click',function(){
            var url="<%=path%>/article/saveOrUpdate";
            var article_pk=$("#article_pk").val();
            getCheck();
            if(ids .length>0) {
                $.ajax({
                    type: 'post',
                    async: false,//同步请求，执行成功后才会继续执行后面的代码
                    url: url,
                    data: {
                        fk_channel_pk:ids,
                        article_pk: article_pk,
                        article_title: $("#article_title").val(),
                        article_sendtime: $("#article_sendtime").val(),
                        article_titleimg: $("#article_titleimg").val(),
                        article_url: $("#article_url").val(),
                        article_content: ue.getContent()
                    },
                    success: function (response) {
                        parent.layer.alert(response.msg);
                        window.location.reload();
                    },
                    error: function (response) {
                        parent.layer.alert(response.msg);
                        window.location.reload();
                    }
                });
            }
            else
            {
                parent.layer.alert("请选择栏目再保存！");
            }
            return false;
        });

    })

</script>
</html>

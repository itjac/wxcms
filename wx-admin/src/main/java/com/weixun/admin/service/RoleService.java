package com.weixun.admin.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.model.SysRole;

import java.util.List;

public class RoleService {


    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String  role_name) {

        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from sys_role s  where 1=1 ");
        if (role_name != null && !role_name.equals("")) {
            sqlstr.append("and s.role_name like '%" + role_name + "%' ");
        }
        sqlstr.append(" order by s.role_pk desc ");

        String select = "select *";

        return Db.paginate(pageNumber, pageSize, select, sqlstr.toString());
    }


    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from sys_role where role_pk in ('"+ids+"')";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }


    /**
     * 数据添加
     * @param username
     * @param password
     * @return
     */
    public boolean add(String username,String password){
        String SQL = "SELECT role_pk FROM sys_role WHERE role_name =?";
        Integer result = Db.queryFirst(SQL, username);
        boolean res=false;
        if(result==null){
            Record record = new Record();
            record.set("user_name", username);
            record.set("user_password", password);
            Db.save("sys_user", record);
            res=true;
        }
        return res;
    }

    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String role_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("select * from sys_role where 1=1 ");
        if (role_pk != null && !role_pk.equals(""))
        {
            stringBuffer.append("and role_pk = '"+role_pk+"'");
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }

    /**
     * 获取列表
     * @return
     */
    public List<SysRole> findModelList(String role_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("select * from sys_role where 1=1 ");
        if (role_pk != null && !role_pk.equals(""))
        {
            stringBuffer.append("and role_pk = '"+role_pk+"'");
        }
//        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        List<SysRole> sysRoles = SysRole.dao.find(stringBuffer.toString());
        return sysRoles;
    }

    /**
     * 获取查询列表
     * @return
     */
    public List<SysRole> List()
    {
        String sql = "select * from sys_role";
        List<SysRole> role = SysRole.dao.find(sql);
        return role;
    }



}

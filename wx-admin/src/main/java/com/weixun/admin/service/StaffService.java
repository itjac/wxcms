package com.weixun.admin.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.model.SysStaff;

import java.util.List;

public class StaffService {

    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String  staff_name,String staff_loginname,String staff_phone,String staff_email) {

        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from sys_staff s left join sys_role r on s.fk_roles_pk=r.role_pk where 1=1 ");
        if (staff_name != null && !staff_name.equals("")) {
            sqlstr.append("and s.staff_name like '%" + staff_name + "%' ");
        }
        if (staff_loginname != null && !staff_loginname.equals("")) {
            sqlstr.append("and s.staff_loginname like '%" + staff_loginname + "%' ");
        }
        if (staff_phone != null && !staff_phone.equals("")) {
            sqlstr.append("and s.staff_phone like '%" + staff_phone + "%' ");
        }
        if (staff_email != null && !staff_email.equals("")) {
            sqlstr.append("and s.staff_email like '%" + staff_email + "%' ");
        }
        sqlstr.append(" order by s.staff_pk desc ");

        String select = "select s.staff_pk,s.staff_name,s.staff_loginname,s.staff_sex,s.staff_birthday,s.staff_email,s.staff_intime,s.staff_status,s.staff_phone,r.role_name as fk_roles_pk";
        return Db.paginate(pageNumber, pageSize, select,sqlstr.toString());

//        String sql = "from sys_staff s left join sys_role r on s.fk_roles_pk=r.role_pk";
//        return Db.paginate(pageNumber, pageSize, "select s.*,r.*",sql.toString());
    }


    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from sys_staff where staff_pk in ("+ids+")";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }


    /**
     * 数据添加
     * @param username
     * @param password
     * @return
     */
    public Record find(String username,String password){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from sys_staff where 1=1 ");
        if (username != null && !username.equals(""))
        {
            stringBuffer.append(" and staff_loginname ='"+username+"'");
        }
        if (password != null && !password.equals(""))
        {
            stringBuffer.append(" and staff_password ='"+password+"'" );
        }
        Record record = Db.findFirst(stringBuffer.toString());
        return record;
    }


    /**
     * 登录用户
     * @param username
     * @param password
     * @return
     */
    public SysStaff findModel(String username,String password){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from sys_staff where 1=1 ");
        if (username != null && !username.equals(""))
        {
            stringBuffer.append(" and staff_loginname ='"+username+"'");
        }
        if (password != null && !password.equals(""))
        {
            stringBuffer.append(" and staff_password ='"+password+"'" );
        }
//        Record record = Db.findFirst(stringBuffer.toString());
        SysStaff sysStaff = SysStaff.dao.findFirst(stringBuffer.toString());
        return sysStaff;
    }

    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String staff_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from sys_staff where 1=1 ");
        if (staff_pk != null && !staff_pk.equals(""))
        {
            stringBuffer.append("and staff_pk = "+staff_pk);
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
//        String sql = "select * from blog";
//        List<Record> list = Db.use("datasource2").query(sql);
        return list;
    }

}
